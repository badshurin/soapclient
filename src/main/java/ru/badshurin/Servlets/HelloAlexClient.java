package ru.badshurin.Servlets;

import ru.badshurin.ws.HelloWebService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

public class HelloAlexClient extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        resp.sendRedirect("http://ya.ru");
        URL url = new URL("http://localhost:1988/wssalex/hellopeople?wsdl");
        QName qName = new QName("http://ws.badshurin.ru/", "HelloWebServiceImplService");
        Service service = Service.create(url, qName);
        HelloWebService hello = service.getPort(HelloWebService.class);
        resp.getWriter().write(hello.getHelloString("Gora"));
    }
}
