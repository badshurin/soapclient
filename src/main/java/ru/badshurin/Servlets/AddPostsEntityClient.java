package ru.badshurin.Servlets;

import ru.badshurin.dao.Main;
import ru.badshurin.dao.NotProbels;
import ru.badshurin.ws.HelloWebService;
import ru.badshurin.ws.PostsEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

public class AddPostsEntityClient extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        NotProbels n = new NotProbels();
        if(n.notProbels(name) || name.length() <= 2) response.sendRedirect("/usernameinvalid");
        else if (n.notProbels(password) || password.length() <= 2) response.sendRedirect("/userpasswordinvalid");
        else
            {

            URL url = new URL("http://localhost:1988/wssalex/hellopeople?wsdl");
            QName qName = new QName("http://ws.badshurin.ru/", "HelloWebServiceImplService");
            Service service = Service.create(url, qName);
            HelloWebService hello = service.getPort(HelloWebService.class);

            PostsEntity user;
            try {
                user = hello.getPostName(name);
            } catch (Exception e) {
                user = null;
            }

            if (user != null) {
                Main.setBackName(name);
                request.getRequestDispatcher("WEB-INF/userexist.jsp").forward(request, response);
            } else {
                hello.addPost(new PostsEntity(name, password));
                response.sendRedirect("/newuser?name=" + name);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
