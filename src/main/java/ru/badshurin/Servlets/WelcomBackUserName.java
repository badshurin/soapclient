package ru.badshurin.Servlets;

import ru.badshurin.dao.Main;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "WelcomBackUserName", urlPatterns = "/backuser")
public class WelcomBackUserName extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        Main.setBackName(name);
        //response.getOutputStream().write(("Welcome back, " + name).getBytes());
        request.getRequestDispatcher("WEB-INF/backexit.jsp").forward(request,response);
    }
}
