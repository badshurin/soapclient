package ru.badshurin.Servlets;

import ru.badshurin.ws.HelloWebService;
import ru.badshurin.ws.PostsEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

@WebServlet(name = "AdminIdServlet", urlPatterns = "/adminid")
public class AdminIdServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        URL url = new URL("http://localhost:1988/wssalex/hellopeople?wsdl");
        QName qName = new QName("http://ws.badshurin.ru/", "HelloWebServiceImplService");
        Service service = Service.create(url, qName);
        HelloWebService hello = service.getPort(HelloWebService.class);

        PostsEntity user;
        try {
            user = hello.getPostId(id);
        } catch (Exception e) {
            user = null;
        }

        if (user != null) {
            hello.deletePostWS(user);
            response.getWriter().write("User with id = " + id + " delete");
        }
        else response.getWriter().write("User with id = " + id + " not exist");

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
